/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/28 21:12:39 by jvuillez          #+#    #+#             */
/*   Updated: 2014/11/21 19:54:06 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <fcntl.h>
#include <stdio.h>

int		main(int ac, char **av)
{
	int		fd;
	int		ret;
	char	*line;

	if (ac >= 2)
	{
		fd = open(av[1], O_RDONLY);
		if (fd == -1)
		{
			printf("open failed\n");
			return (0);
		}
	}
	else
		fd = 0;
	while ((ret = get_next_line(fd, &line)) == 1)
	{
		printf("line : %s		%i\n", line, ret);
		free(line);
	}
	close(fd);
	return (0);
}
