/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/28 21:19:35 by jvuillez          #+#    #+#             */
/*   Updated: 2014/09/28 23:33:59 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(const char *s1, const char *s2)
{
	char	*str;
	int		i;
	int		j;

	i = 0;
	if (!(str = malloc(sizeof(char) * (ft_strlen(s1) + ft_strlen(s2) + 1))))
		return (NULL);
	if (s1 != NULL)
		while (s1[i] != '\0')
		{
			str[i] = s1[i];
			i++;
		}
	j = 0;
	if (s2 != NULL)
		while (s2[j] != '\0')
		{
			str[i] = s2[j];
			i++;
			j++;
		}
	str[i] = '\0';
	return (str);
}
