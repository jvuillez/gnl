# README #

### What is this repository for? ###

Version finale

Simple fonction qui renvoie la ligne
(aka jusqu'au prochain \n)
peu importe la taille du READ effectué

### HOW TO ###

Main.c n'est écrit que pour proposer un moyen de tester

Un appel à get_next_line(int const fd, char **line)
avec fd le fichier à lire (0 pour l'entrée standard ; sinon le fd return par OPEN)
et avec line l'adresse dans laquelle mettre le pointeur vers la chaine de caractère retournée

permet d'obtenir (par appels à READ de la taille de BUFF_SIZE (défini dans get_next_line.h))
la prochaine ligne du fichier.