/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvuillez <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/09/23 10:50:01 by jvuillez          #+#    #+#             */
/*   Updated: 2014/09/28 23:39:04 by jvuillez         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		ft_checkbuff(char *str)
{
	int		i;

	i = 0;
	if (str == NULL)
		return (0);
	while (*str != '\0')
	{
		if (*str == '\n')
			i++;
		str++;
	}
	return (i);
}

void	ft_buftoline(char *staticbuf, char **line)
{
	int		i;

	i = 0;
	(*line) = malloc(sizeof(char) * ft_strlen(staticbuf));
	while (staticbuf[i] != '\0' && staticbuf[i] != '\n')
	{
		(*line)[i] = staticbuf[i];
		i++;
	}
	(*line)[i] = '\0';
}

char	*ft_bufmanager(char *buf)
{
	char	*str;
	char	*save;
	int		i;

	i = 0;
	save = ft_strdup(buf);
	str = save;
	free(buf);
	while (*str != '\n')
		str++;
	if (*str == '\n')
		str++;
	buf = malloc(sizeof(char) * ft_strlen(str));
	while (str[i] != '\0')
	{
		buf[i] = str[i];
		i++;
	}
	buf[i] = '\0';
	free(save);
	return (buf);
}

int		get_next_line(int const fd, char **line)
{
	static char		*staticbuf;
	char			buf[BUFF_SIZE + 1];
	int				size;

	if (ft_checkbuff(staticbuf) != 0)
	{
		ft_buftoline(staticbuf, line);
		staticbuf = ft_bufmanager(staticbuf);
		return (1);
	}
	while (ft_checkbuff(staticbuf) == 0)
	{
		if ((size = read(fd, buf, BUFF_SIZE)) == -1)
			return (-1);
		if (size == 0)
		{
			if (!staticbuf || staticbuf[0] == '\0')
				return (0);
			ft_buftoline(staticbuf, line);
			return (staticbuf[0] = '\0', 1);
		}
		buf[size] = '\0';
		staticbuf = ft_strjoin(staticbuf, buf);
	}
	return (get_next_line(fd, line));
}
